const limitFunctionCallCount = require('../limitFunctionCallCount')

const result =limitFunctionCallCount(cb=(c)=>{
    
    console.log("Invoked Callback  "+c+"  times!")    }, 5);                                          //n=5

result() //Invoked Callback  1 times!

result() //Invoked Callback  2  times!

result() //Invoked Callback  3  times!

result() //Invoked Callback  4  times!

result() //Invoked Callback  5  times!

result() // return null (count exceeded than n cb called)

result() // return null (count exceeded than n cb called)

