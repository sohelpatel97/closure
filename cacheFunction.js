// Using Set Object cacheFunction

const cacheFunction = (cb) =>{

    let cache = {} 

    const invokerFunction = (argument)=>{

        if (cache.hasOwnProperty(argument)) {

            console.log(cache);
            
            return cache[argument]+" Already In Cache"; // Return if Parameter  in cache
        }
        
        cache[argument] = cb(argument); //Storing first time in cache

        return cache[argument];


    }

    return invokerFunction
}


module.exports = cacheFunction;
