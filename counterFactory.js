function counterFactory(){
    
    let value = 0;

    const increment = ()=>{value++; return value}

    const decrement = ()=>{value--; return value}
    
    const result = { increment, decrement }

    return result
  }

  module.exports = counterFactory



